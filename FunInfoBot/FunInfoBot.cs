﻿using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using BotScaffold;
using System.Text;
using System.Collections.Generic;

namespace FunBot
{
    /// <summary>
    /// A bot that provides a few useful utilities for grabbing useful or fun info.
    /// </summary>
    public class FunInfoBot : BotInstance.Bot<FunInfoBotConfig>
    {
        /// <summary>
        /// Creates a new instance of the hangout bot.
        /// </summary>
        /// <param name="details">The client details to construct the bot with.</param>
        public FunInfoBot(string name) : base(name)
        {

        }

        /// <summary>
        /// Returns a list of all users with a role.
        /// </summary>
        /// <param name="args">The command arguments.</param>
        /// <returns>A task for completing the command.</returns>
        [Usage("Role Users.")]
        [Argument("Role Name", "The name of the role you want to lookup.")]
        [Command("role users", CommandLevel = CommandLevel.Unrestricted, ParameterRegex = "^ (?<role>[^@].*)$")]
        protected async Task RoleUsers(CommandArgs<FunInfoBotConfig> args)
        {
            const int MAXSIZE = 2000;
            if (args.Config.HelpChannelID == args.Channel.Id)
            {
                // Checking if any role matches the given name
                string roleName = args["role"];
                ulong roleId = 0;

                foreach(KeyValuePair<ulong, DiscordRole> role in args.Guild.Roles)
                {
                    if (role.Value.Name == roleName)
                    {
                        // Found it!
                        roleId = role.Key;
                        break;
                    }
                }

                if (roleId == 0)
                {
                    await args.Channel.SendMessageAsync($"Couldn't locate role `{roleName}`.");
                }
                else
                {
                    // Finding all users with this role
                    StringBuilder sb = new StringBuilder($"All users with role `{roleName}`:\n```\n", MAXSIZE);
                    bool foundUser = false;

                    IReadOnlyCollection<DiscordMember> members = await args.Guild.GetAllMembersAsync();

                    foreach(DiscordMember user in members)
                    {
                        // Checking if this user has this role
                        bool hasRole = false;
                        foreach(DiscordRole role in user.Roles)
                        {
                            if (role.Id == roleId)
                            {
                                // Found it!
                                foundUser = true;
                                hasRole = true;
                                break;
                            }
                        }
                        if (hasRole) {
                            // Splitting message if needed
                            // 4 is the length of \n + ```
                            if (sb.Length + user.DisplayName.Length + 4 > MAXSIZE)
                            {
                                // Send it now then start over
                                sb.Append("\n```");
                                await args.Channel.SendMessageAsync(sb.ToString());
                                sb.Clear();
                                sb.Append("```\n");
                            }
                            sb.Append(user.DisplayName);
                            sb.Append("\n");
                        }
                    }
                    // Finally finishing off and sending the final message, if users were found
                    if (foundUser)
                    {
                        sb.Append("\n```");
                        await args.Channel.SendMessageAsync(sb.ToString());
                    }
                    else {
                        await args.Channel.SendMessageAsync("No members with that role.");
                    }
                }
            }
        }
        

        /// <summary>
        /// Creates a default config data structure for new servers.
        /// </summary>
        /// <returns>a config data structure.</returns>
        protected override FunInfoBotConfig CreateDefaultConfig()
        {
            return new FunInfoBotConfig('!');
        }
    }
}