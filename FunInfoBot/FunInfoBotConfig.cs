using BotScaffold;

namespace FunBot
{
    /// <summary>
    /// Represents fun info bot configuration information for a specific server.
    /// </summary>
    public class FunInfoBotConfig : BotConfig
    {
        /// <summary>
        /// Creates a new fun info bot config object using the specified indicator.
        /// </summary>
        /// <param name="indicator">The character all commands for this bot must begin with.</param>
        public FunInfoBotConfig(char indicator) : base(indicator)
        {
            
        }
    }
}